import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
  {
    path: '/mylogin',
    name: 'myLogin',
    component: () => import('../views/myLogin.vue')
  },{
    path: '/',
    name: 'myHome',
    component: () => import('../views/home/home.vue'),
    children: [
      {
        path: '/homePage',
        name: 'homePage',
        component: () => import('../views/home/homePage.vue')
      },
      {
        path: '/light',
        name: 'lightController',
        component: () => import('../views/home/lightController.vue')
      },
      {
        path: '/tem',
        name: 'temController',
        component: () => import('../views/home/temController.vue')
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
